#!/usr/bin/env python
# encoding: utf-8

'''
@version: 4.1
@author: szerelme
@license:
@contact: chenj@cqporun.com
@site: http://www.yinka.co
@software: PyCharm
@file: errorcode.py
@time: 2016/8/24 10:27
'''

code = {
    '10000': {
        '10001': '获取允许访问终端列表失败',
    },
    '20000': {
        '20001': '禁止终端访问失败',
    },
}
