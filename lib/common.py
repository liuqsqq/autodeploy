#!/usr/bin/env python
# encoding: utf-8

'''
@version: 4.1
@author: szerelme
@license:
@contact: chenj@cqporun.com
@site: http://www.yinka.co
@software: PyCharm
@file: common.py
@time: 2016/8/24 10:27
'''

from tornado.escape import json_encode
from errorcode import code as errror_code

def callback_data(result=1, code='', **kwargs):
    '''
    @返回参数处理
    @param result:接口处理状态（1成功，0失败）
    @param code：错误编码
    @param kwargs：返回的数据
    '''
    data = dict(
        {
            'message': '处理成功',
            'result': 1
        }
    )
    try:
        message = '处理失败'

        if result == 0:
            if code:
                message = errror_code[code[:1] + '0000'][code]
            data['code'] = code
            data['message'] = message
            data['result'] = 0
        if kwargs:
            if 'callback_list' in kwargs.keys():
                data['body'] = kwargs['callback_list']
            else:
                data['body'] = kwargs
    except Exception as e:
        data = dict(
            {
                'message': '处理失败',
                'result': 0
            }
        )
    finally:
        return json_encode(data)