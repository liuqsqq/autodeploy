#!/usr/bin/env python
# encoding: utf-8

'''
@version: 4.1
@author: szerelme
@license:
@contact: chenj@cqporun.com
@site: http://www.yinka.co
@software: PyCharm
@file: base.py
@time: 2016/8/24 10:27
'''

import tornado.web

class BaseHandler(tornado.web.RequestHandler):
    def __init__(self, *argc, **argkw):
        super(BaseHandler, self).__init__(*argc, **argkw)

    @property
    def autodeploy_model(self):
        return self.application.autodeploy_model