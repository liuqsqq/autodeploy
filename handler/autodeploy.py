import tornado.web
import tornado.gen
from .base import BaseHandler
import tornado.escape as escape
import json
import lib.common as common
class CodeRemindHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        try:

            data_string = yield self.autodeploy_model.fnPush(escape.json_decode(self.request.body))
            yield self.autodeploy_model.send_message_to_dingtalk(data_string)
        except Exception as e:
            print ("Exception occur in CodeRemindHandler %s" % e)
        finally:
            self.finish()

class GetCtlTokenHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        ret = True
        try:

            data_string = yield self.autodeploy_model.get_ctl_result(escape.json_decode(self.request.body))
        except Exception as e:
            print("Exception occur in GetCtlTokenHandler %s" % e)
            ret = False
        finally:
            if ret:
                self.finish(common.callback_data(1, '', callback_list=data_string))
            else:
                self.finish(common.callback_data(0, 10001))

class DiableAccessTerminalnHandler(BaseHandler):
    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        ret = True
        try:

            yield self.autodeploy_model.disable_remote_access_by_terminal_name(escape.json_decode(self.request.body))
        except Exception as e:
            print ("Exception occur in DiableAccessTerminalnHandler %s" % e)
            ret = False
        finally:
            if ret:
                self.finish(common.callback_data(1, '', callback_list=""))
            else:
                self.finish(common.callback_data(0, 20001))