import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options
import handler.autodeploy
from lib.loader import Loader
import traceback

define("port", default=9002, help="run on the given port", type=int)

class Application(tornado.web.Application):
    def __init__(self, ltobj):
        handlers = [
            (r"/terminal/coderemind", handler.autodeploy.CodeRemindHandler),
            (r"/terminal/getctltoken", handler.autodeploy.GetCtlTokenHandler),
            (r"/terminal/diableaccessterminal", handler.autodeploy.DiableAccessTerminalnHandler),
        ]
        tornado.web.Application.__init__(self, handlers)
        try:
            for k in ltobj:
                setattr(self, k, ltobj[k])
            print('初始化Object成功...')
        except Exception as e:
            print('初始化Object失败')
            print('Error:%s' % e)
            return
def main():
        ltobj = {}
        try:
            loader = Loader()
            ltobj['loader'] = loader
            ltobj['autodeploy_model'] = loader.use('autodeploy.model')

            tornado.options.parse_command_line()
            http_server = tornado.httpserver.HTTPServer(Application(ltobj), xheaders=True)
            http_server.listen(options.port)
            tornado.ioloop.IOLoop.instance().start()
        except Exception as e:
            print("main 异常:",e)
            print('traceback.format_exc():\n%s' % traceback.format_exc())
if __name__ == "__main__":
    main()